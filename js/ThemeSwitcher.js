document.addEventListener('DOMContentLoaded', function () {
    const body = document.body;
    const colorButton = document.getElementById('colorButton');

    // Función para cambiar entre modos
    function toggleMode() {
        body.classList.toggle('dark-mode');
        body.classList.toggle('light-mode');
    }

    // Agregar evento al botón de cambio de color
    colorButton.addEventListener('click', toggleMode);

    // Detectar la preferencia de color del sistema
    const prefersDarkMode = window.matchMedia('(prefers-color-scheme: dark)');
    
    // Aplicar tema inicial según la preferencia del sistema
    if (prefersDarkMode.matches) {
        body.classList.add('dark-mode');
    } else {
        body.classList.add('light-mode');
    }

    // Actualizar el tema si cambia la preferencia del sistema
    prefersDarkMode.addEventListener('change', () => {
        if (prefersDarkMode.matches) {
            body.classList.remove('light-mode');
            body.classList.add('dark-mode');
        } else {
            body.classList.remove('dark-mode');
            body.classList.add('light-mode');
        }
    });
});
